from pycbrf.toolbox import ExchangeRates
from pytz import timezone
from datetime import datetime


BROKER_ACCOUNT_STARTED = datetime(2020, 1, 1, 0, 0, 0)


def _get_tz():
    return timezone('Europe/Moscow')


def localize():
    return _get_tz().localize(BROKER_ACCOUNT_STARTED)


def get_now():
    return _get_tz().localize(datetime.now())


def get_usd():
    rates = ExchangeRates(get_now().strftime('%Y-%m-%d'))
    usd = round(rates['USD'].value, 2)
    return usd


if __name__ == '__main__':
    print(get_usd())
