import tinvest
from pprint import pprint
from decimal import Decimal


from config import TI_TOKEN, BROKER, BROKER_IIS
from uttils import get_usd, get_now, localize

CLIENT = tinvest.SyncClient(TI_TOKEN)


def _get_user_account():
    """
    Получение broker_account_id
    """
    api = tinvest.UserApi(CLIENT)
    response = api.accounts_get()

    if response.status_code == 200:
        pprint(response.parse_json().payload.accounts)


def get_cash(broker_account_id):
    """
    Получаем размер свободных денежных средств

    :param broker_account_id: str
    :return: Decimal
    """
    api = tinvest.PortfolioApi(CLIENT)
    response = api.portfolio_currencies_get(broker_account_id=broker_account_id)

    currencies = response.parse_json().payload.currencies

    sum_cash = Decimal('0')

    for cur in currencies:
        if cur.currency == 'USD':
            sum_cash += Decimal(str(cur.balance)) \
                        * Decimal(str(get_usd()))
        else:
            sum_cash += Decimal(str(cur.balance))

    return sum_cash


def account_value(broker_account_id):
    """
    Считаем стоимость портфеля

    :param broker_account_id: str
    :return: Decimal
    """
    api = tinvest.PortfolioApi(CLIENT)
    response = api.portfolio_get(broker_account_id=broker_account_id)

    positions = response.parse_json().payload.positions

    active_money = Decimal('0')
    for position in positions:
        if position.average_position_price.currency == 'USD':
            active_money += Decimal(str(position.average_position_price.value)) \
                            * Decimal(str(get_usd())) \
                            * Decimal(str(position.balance)) \
                            + Decimal(str(position.expected_yield.value))
        else:
            active_money += Decimal(str(position.average_position_price.value)) \
                            * Decimal(str(position.balance)) \
                            + Decimal(str(position.expected_yield.value))

    active_money = active_money + get_cash(broker_account_id)
    return active_money


def deposit_money(broker_account_id):
    """
    Подсчитываем внесенные деньги

    :param broker_account_id: str
    :return: Decimal
    """
    api = tinvest.OperationsApi(CLIENT)
    response = api.operations_get(from_=localize(), to=get_now(), broker_account_id=broker_account_id)

    moneys = response.parse_json().payload.operations
    money_cash = Decimal('0')
    for money in moneys:
        if money.operation_type == 'PayIn':
            if money.currency == 'USD':
                money_cash += Decimal(str(money.payment)) * Decimal(str(get_usd()))
            else:
                money_cash += Decimal(str(money.payment))

    return money_cash


def get_percent(broker_account_id):
    money_sum = account_value(broker_account_id)
    money_deposit = deposit_money(broker_account_id)
    profit_in_rub = money_sum - money_deposit
    profit_in_percent = 100 * profit_in_rub / money_deposit
    print(f"Наши пополнения: {round(money_deposit, 2)} руб\n"
          f"Стоимость портфеля на текущий момент: {round(money_sum, 2)} руб\n"
          f"Прибыль: {round(profit_in_rub, 2)} руб\n"
          f"Прибыль: {round(profit_in_percent, 2)} %")


if __name__ == '__main__':
    # get_info_broker(broker_account_id=BROKER)
    # print(account_value(broker_account_id=BROKER_IIS))
    # print(get_cash(broker_account_id=BROKER_IIS))
    # print(deposit_money(broker_account_id=BROKER))
    get_percent(broker_account_id=BROKER_IIS)
